package uk.co.cichocki.guava;

import com.google.common.collect.Ordering;
import lombok.extern.log4j.Log4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 02/08/2016.
 */
@Log4j
public class OrederingTester implements GuavaTester {
    public Object justdoit(Object object) {

        List<Integer> numbers = new ArrayList<Integer>();

        numbers.add(5);
        numbers.add(2);
        numbers.add(15);
        numbers.add(51);
        numbers.add(53);
        numbers.add(35);
        numbers.add(45);
        numbers.add(32);
        numbers.add(43);
        numbers.add(16);

        Ordering ordering = Ordering.natural();
        log.debug("Input List: ");
        log.debug(numbers);

        Collections.sort(numbers,ordering );
        log.debug("Sorted List: ");
        log.debug(numbers);

        log.debug("======================");
        log.debug("List is sorted: " + ordering.isOrdered(numbers));
        log.debug("Minimum: " + ordering.min(numbers));
        log.debug("Maximum: " + ordering.max(numbers));

        Collections.sort(numbers,ordering.reverse());
        log.debug("Reverse: " + numbers);

        numbers.add(null);
        log.debug("Null added to Sorted List: ");
        log.debug(numbers);

        Collections.sort(numbers,ordering.nullsFirst());
        log.debug("Null first Sorted List: ");
        log.debug(numbers);
        log.debug("======================");

        List<String> names = new ArrayList<String>();

        names.add("Ram");
        names.add("Shyam");
        names.add("Mohan");
        names.add("Sohan");
        names.add("Ramesh");
        names.add("Suresh");
        names.add("Naresh");
        names.add("Mahesh");
        names.add(null);
        names.add("Vikas");
        names.add("Deepak");

        log.debug("Another List: ");
        log.debug(names);

        Collections.sort(names,ordering.nullsFirst().reverse());
        log.debug("Null first then reverse sorted list: ");
        log.debug(names);

        return null;
    }
}
