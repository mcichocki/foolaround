package uk.co.cichocki;

import lombok.extern.log4j.Log4j;

/**
 * Created by user on 02/08/2016.
 */
@Log4j
public class Launcher {
    public static void main(String[] args){
        log.trace("test 1");
        log.debug("test 2");
        log.warn("test 3");
        log.fatal("test 4");
        log.info("test 5");
    }
}
